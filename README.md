<h1>Tibber backend developer test solution</h1>

By Mikael Örn

<h3>Setup instructions</h3>
run docker-compose up in root dir. Api is available on localhost:5000. swagger ui is on localhost:5000/swagger

<h3>Description</h3>
Dev environment using docker-compose with postgres db.
ASP.NET 5 WebApi using n tier architecture with 3 layers:
API, Domain, Repository.
Have only added comments in the code where i would have in real app. so if there are any questions lets discuss them. Exception being RouteCalculator where i wanted to describe my reasoning.
Lots of boilerplate for simple problem and the instructions said keep it simple, but chose to focus on maintainability and SOLID. Saw it more as a 
full microservice expected to grow, otherwise it could have been a lambda func or similar.

<h3>Dependencies</h3>
<ul>
    <li>NpgSql for balance between functionality and simplicity. Had to implement poor mans DbContext for DI but i think its working nicely</li>
    <li>xUnit since i couldnt get mstest to work with .Net 5</li>
</ul>

<h3>Assumptions outside the instructions</h3>
<ul>
    <li>Caller is always trusted so API always returns full error details.</li>
    <li>Duration is time for actual execution of commands. for time between request and response you should calculate total internal processing time in some request interceptor middleware</li>
</ul>


<h3>Limitations</h3>
<ul>
    <li>Microservice best practices like circuit breaker, retries etc</li>
    <li>Demonstrated env var configurability with only db password and host. default asp.net setup enables env vars to override appsettings so you can just add more</li>
    <li>Couldnt set internal container port to 5000 when running in container. but external port is 5000</li>
    <li>Not very experienced with postgres so left it mostly default</li>
    <li>Included sensitive env vars in repo only because its an assignment</li>
    <li>Only tested on windows</li>
    <li>Performance is very good with max amount of commands but not great with max commands and steps. Probably some kickass algorithm i dont know can fix this</li>
</ul>