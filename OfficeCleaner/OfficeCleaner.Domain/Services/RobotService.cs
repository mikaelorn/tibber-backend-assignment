﻿using OfficeCleaner.Domain.Models;
using OfficeCleaner.Domain.Services.Interfaces;
using OfficeCleaner.Domain.Utils;
using OfficeCleaner.Repository.Interfaces;
using System.Threading.Tasks;

namespace OfficeCleaner.Domain.Services
{
    public class RobotService : IRobotService
    {
        private readonly IExecutionRepository _repository;
        public RobotService(IExecutionRepository repository)
        {
            _repository = repository;
        }

        public async Task<CleaningResultModel> Clean(CleaningInstructionsModel instructions)
        {
            var cleaningResult = RouteCalculator.PerformCleaning(instructions);
            var dbModel = cleaningResult.ToDbModel();

            var repositoryResponse = await _repository.Add(dbModel);
            var domainModel = repositoryResponse.ToDomainModel();

            return domainModel;
        }
    }
}
