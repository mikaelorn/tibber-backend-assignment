﻿using OfficeCleaner.Domain.Models;
using System.Threading.Tasks;

namespace OfficeCleaner.Domain.Services.Interfaces
{
    public interface IRobotService
    {
        Task<CleaningResultModel> Clean(CleaningInstructionsModel instructions);
    }
}
