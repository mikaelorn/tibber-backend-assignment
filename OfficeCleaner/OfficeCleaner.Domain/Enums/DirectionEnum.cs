﻿using System.Text.Json.Serialization;

namespace OfficeCleaner.Domain.Enums
{
    [JsonConverter(typeof(JsonStringEnumConverter))]
    public enum DirectionEnum
    {
        North,
        East,
        South,
        West
    }
}
