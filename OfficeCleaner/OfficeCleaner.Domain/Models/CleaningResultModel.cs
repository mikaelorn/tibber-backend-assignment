﻿using System;

namespace OfficeCleaner.Domain.Models
{
    public class CleaningResultModel
    {
        public DateTime Timestamp { get; set; }
        public int Commands { get; set; }
        public int Result { get; set; }
        public decimal Duration { get; set; }
    }
}
