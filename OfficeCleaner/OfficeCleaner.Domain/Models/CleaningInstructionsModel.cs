﻿using OfficeCleaner.Domain.Enums;
using System;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace OfficeCleaner.Domain.Models
{
    public class CleaningInstructionsModel
    {
        public Start Start { get; set; }
        public Command[] Commands { get; set; }
    }

    public class Start
    {
        [Range(-100000, 100000,
            ErrorMessage = "Value for {0} must be between {1} and {2}.")]
        public int x { get; set; }

        [Range(-100000, 100000,
            ErrorMessage = "Value for {0} must be between {1} and {2}.")]
        public int y { get; set; }
    }

    public class Command
    {
        [JsonConverter(typeof(JsonStringEnumConverter))]
        public DirectionEnum Direction { get; set; }

        [Range(0, 100000,
            ErrorMessage = "Value for {0} must be between {1} and {2}.")]
        public int Steps { get; set; }
    }
}
