﻿using OfficeCleaner.Domain.Models;
using OfficeCleaner.Repository.Models;

namespace OfficeCleaner.Domain.Utils
{
    internal static class Mapper
    {
        internal static Execution ToDbModel(this CleaningResultModel r)
        {
            return new()
            {
                Commands = r.Commands,
                Result = r.Result,
                Duration = r.Duration
            };
        }

        internal static CleaningResultModel ToDomainModel(this Execution e)
        {
            return new()
            {
                Commands = e.Commands,
                Result = e.Result,
                Duration = e.Duration,
                Timestamp = e.ExecutionTimestamp
            };
        }
    }
}
