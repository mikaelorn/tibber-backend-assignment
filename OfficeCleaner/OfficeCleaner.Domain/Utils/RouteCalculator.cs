﻿using OfficeCleaner.Domain.Enums;
using OfficeCleaner.Domain.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace OfficeCleaner.Domain.Utils
{
    public static class RouteCalculator
    {

        /// <summary>
        /// Readability is not as good as it could be here
        /// Focused completely on performance
        /// </summary>
        /// <param name="instructions"></param>
        /// <returns></returns>
        public static CleaningResultModel PerformCleaning(CleaningInstructionsModel instructions)
        {
            CleaningResultModel cleaningResult = new()
            {
                Result = 0,
                Commands = instructions.Commands.Length
            };

            if (instructions.Commands.Length < 1)
                return cleaningResult;

            Stopwatch watch = new();
            watch.Start();

            //Strip commands without steps
            var nonEmptyCmds = instructions.Commands.Where(c => c.Steps != 0).ToList();
            var cmdsWithStartPos = GenerateCommandsWithStartPos(instructions.Start, nonEmptyCmds);
            var set = ExecuteCommandsParallel(cmdsWithStartPos);

            cleaningResult.Result = set.Count;

            watch.Stop();
            cleaningResult.Duration = Convert.ToDecimal(watch.Elapsed.TotalMilliseconds) / 1000;

            return cleaningResult;
        }

        /// <summary>
        ///Preprocess into commands with its own start pos calculated from all commands and steps before.
        ///Enables parallel command execution.
        ///takes a few ms with 10000 commands.
        ///Returns tuple with command and its start pos.
        /// </summary>
        /// <param name="start"></param>
        /// <param name="commands"></param>
        /// <returns></returns>
        private static (Start, Command)[] GenerateCommandsWithStartPos(Start start, List<Command> commands)
        {
            //array of tuples
            var processedCommands = new (Start, Command)[commands.Count];

            //Keep track of current pos in grid
            int currentX = start.x;
            int currentY = start.y;

            //set first so we get start pos
            processedCommands[0] = (start, commands[0]);
            SetNewPos(commands[0]);

            //Iterate over commands in order
            for (int i = 1; i < commands.Count; i++)
            {
                var command = commands[i];

                if (command.Steps == 0)
                    continue;

                processedCommands[i] = new(new Start { x = currentX, y = currentY }, command);

                SetNewPos(command);
            }

            return processedCommands;

            //Local func to update correct coordinate
            void SetNewPos(Command command)
            {
                switch (command.Direction)
                {
                    case DirectionEnum.North:
                        currentY += command.Steps;
                        break;
                    case DirectionEnum.East:
                        currentX += command.Steps;
                        break;
                    case DirectionEnum.West:
                        currentX -= command.Steps;
                        break;
                    case DirectionEnum.South:
                        currentY -= command.Steps;
                        break;
                    default:
                        break;
                }
            }
        }

        /// <summary>
        /// Takes set of commands where order doesnt matter since it each has its correct start pos.
        /// </summary>
        /// <param name="commandsWithPos"></param>
        /// <returns></returns>
        private static HashSet<(int, int)> ExecuteCommandsParallel((Start, Command)[] commandsWithPos)
        {
            //Hashset can only hold unique values so 0 risk of doubles
            var visitedTiles = new HashSet<(int, int)>();
            //Set start tile as visited
            visitedTiles.Add((commandsWithPos.First().Item1.x, commandsWithPos.First().Item1.y));

            //cant add to collection in parallel loop without locking
            Object lockMe = new();

            //hogs all the processors it can and brute forces each command
            //cut my tests in half for larger datasets but not great for smaller.
            //could add logic to run smaller sets in sequence
            Parallel.For(0, 
                commandsWithPos.Length, 
                new ParallelOptions() { MaxDegreeOfParallelism = Environment.ProcessorCount }, 
                i =>
            {
                //This commands unique visited tiles, current coordinates
                var set = new HashSet<(int, int)>();
                int currentX = commandsWithPos[i].Item1.x;
                int currentY = commandsWithPos[i].Item1.y;
                var command = commandsWithPos[i].Item2;

                switch (command.Direction)
                {
                    case DirectionEnum.North:
                        for (int j = 0; j < command.Steps; j++)
                        {
                            currentY++;
                            set.Add((currentX, currentY));
                        }
                        break;
                    case DirectionEnum.East:
                        for (int j = 0; j < command.Steps; j++)
                        {
                            currentX++;
                            set.Add((currentX, currentY));
                        }
                        break;
                    case DirectionEnum.West:
                        for (int j = 0; j < command.Steps; j++)
                        {
                            currentX--;
                            set.Add((currentX, currentY));
                        }
                        break;
                    case DirectionEnum.South:
                        for (int j = 0; j < command.Steps; j++)
                        {
                            currentY--;
                            set.Add((currentX, currentY));
                        }
                        break;
                    default:
                        break;
                }

                //Lock and union with total visited. No doubles but prob wasteful to iterate every step
                lock (lockMe)
                {
                    visitedTiles.UnionWith(set);
                }
            });

            return visitedTiles;
        }
    }
}