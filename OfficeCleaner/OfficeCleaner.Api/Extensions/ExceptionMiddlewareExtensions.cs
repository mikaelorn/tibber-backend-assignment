﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Http;
using OfficeCleaner.Api.Models;
using System.Net;

namespace OfficeCleaner.Api.Extensions
{
    public static class ExceptionMiddlewareExtensions
    {
        public static void ConfigureExceptionHandler(this IApplicationBuilder app)
        {
            app.UseExceptionHandler(appError =>
            {
                appError.Run(async context =>
                {
                    context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                    context.Response.ContentType = "application/json";
                    var contextFeature = context.Features.Get<IExceptionHandlerFeature>();
                    if (contextFeature != null)
                    {
                        //TODO Log to central logging solution

                        await context.Response.WriteAsync(new ErrorModel()
                        {
                            Status = context.Response.StatusCode,
                            Message = contextFeature.Error.Message,
                            Details = contextFeature.Error.StackTrace
                        }.ToString());
                    }
                });
            });
        }
    }
}
