using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using OfficeCleaner.Api.Extensions;
using OfficeCleaner.Domain.Services;
using OfficeCleaner.Domain.Services.Interfaces;
using OfficeCleaner.Repository;
using OfficeCleaner.Repository.Interfaces;
using OfficeCleaner.Repository.Models;
using OfficeCleaner.Repository.Repositories;

namespace OfficeCleaner.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "OfficeCleaner.Api", Version = "v1" });
            });

            services.Configure<DatabaseSettingsModel>(Configuration.GetSection("DatabaseConnection"));

            services.AddScoped<IRobotService, RobotService>();
            services.AddScoped<IExecutionRepository,ExecutionRepository>();
            services.AddSingleton<IDatabaseContext, DatabaseContext>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseSwagger();
            app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "OfficeCleanerApi v1"));

            app.ConfigureExceptionHandler();
            app.UseRouting();
            app.UseEndpoints(endpoints => endpoints.MapControllers());
        }
    }
}
