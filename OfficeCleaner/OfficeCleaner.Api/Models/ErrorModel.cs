﻿using System.Text.Json;

namespace OfficeCleaner.Api.Models
{
    public class ErrorModel
    {
        public string Message { get; set; }
        public int Status { get; set; }
        public string Details { get; set; }

        public override string ToString()
        {
            return JsonSerializer.Serialize(this);
        }
    }
}
