﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using OfficeCleaner.Domain.Models;
using OfficeCleaner.Domain.Services.Interfaces;
using System.Threading.Tasks;

namespace OfficeCleaner.Api.Controllers
{
    [ApiController]
    [Route("tibber-developer-test")]
    public class RobotController : ControllerBase
    {
        private readonly ILogger<RobotController> _logger;
        private readonly IRobotService _robotService;

        public RobotController(ILogger<RobotController> logger, IRobotService robotService)
        {
            _logger = logger;
            _robotService = robotService;
        }

        [HttpPost]
        [Route("enter-path")]
        public async Task<CleaningResultModel> Post([FromBody] CleaningInstructionsModel instructions)
        {
            return await _robotService.Clean(instructions);
        }
    }
}
