using OfficeCleaner.Domain.Enums;
using OfficeCleaner.Domain.Models;
using OfficeCleaner.Domain.Utils;
using Xunit;

namespace OfficeCleaner.Tests
{
    public class RouteCalculatorUnitTests
    {
        [Fact]
        public void PerformCleaning_NoCommands_ReturnsZero()
        {
            CleaningInstructionsModel instructions = new()
            {
                Commands = new Command[] { }
            };

            var cleaningResult = RouteCalculator.PerformCleaning(instructions);

            Assert.Equal(0, cleaningResult.Result);
        }

        [Fact]
        public void PerformCleaning_OneCommandOneStep_ReturnsTwo()
        {
            CleaningInstructionsModel instructions = new() 
            {
                Start = new Start{ x = 5, y = 3 },
                Commands = new Command[] {
                    new Command{ Direction = DirectionEnum.East, Steps = 1 }
                }
            };

            var cleaningResult = RouteCalculator.PerformCleaning(instructions);

            Assert.Equal(2, cleaningResult.Result);
        }

        [Fact]
        public void PerformCleaning_NegativeStart_ReturnsTwo()
        {
            CleaningInstructionsModel instructions = new() 
            {
                Start = new Start{ x = -23094, y = -76511 },
                Commands = new Command[] {
                    new Command{ Direction = DirectionEnum.East, Steps = 1 }
                }
            };

            var cleaningResult = RouteCalculator.PerformCleaning(instructions);

            Assert.Equal(2, cleaningResult.Result);
        }

        [Fact]
        public void PerformCleaning_DoubleVisits_ReturnsOneResult()
        {
            CleaningInstructionsModel instructions = new()
            {
                Start = new Start{ x = 5, y = 3,},
                Commands = new Command[] {
                    new Command{ Direction = DirectionEnum.East, Steps = 1 },
                    new Command{ Direction = DirectionEnum.West, Steps = 1 }
                }
            };

            var cleaningResult = RouteCalculator.PerformCleaning(instructions);

            Assert.Equal(2, cleaningResult.Result);
        }

        [Fact]
        public void PerformCleaning_LongStraight_ReturnsSameAsSteps()
        {
            CleaningInstructionsModel instructions = new()
            {
                Start = new Start { x = 0, y = 0, },
                Commands = new Command[] {
                    new Command { Direction = DirectionEnum.East,Steps = 100000 }
                }
            };

            var cleaningResult = RouteCalculator.PerformCleaning(instructions);

            Assert.Equal(100001, cleaningResult.Result);
        }

        [Fact]
        public void PerformCleaning_ManyDoubles_ReturnsHalfSteps()
        {
            CleaningInstructionsModel instructions = new()
            {
                Start = new Start { x = 0, y = 0, },
                Commands = new Command[] {
                    new Command{ Direction = DirectionEnum.East, Steps = 100000 },
                    new Command{ Direction = DirectionEnum.West, Steps = 100000 }
                },
            };

            var cleaningResult = RouteCalculator.PerformCleaning(instructions);

            Assert.Equal(100001, cleaningResult.Result);
        }

        [Fact]
        public void PerformCleaning_ZeroSteps_ReturnsOk()
        {
            CleaningInstructionsModel instructions = new()
            {
                Start = new Start { x = 0, y = 0, },
                Commands = new Command[] {
                    new Command { Direction = DirectionEnum.East, Steps = 0 },
                    new Command { Direction = DirectionEnum.West, Steps = 100000 }
                },
            };

            var cleaningResult = RouteCalculator.PerformCleaning(instructions);

            Assert.Equal(100001, cleaningResult.Result);
        }

        [Fact]
        public void PerformCleaning_MediumComplex_ReturnsOk()
        {
            CleaningInstructionsModel instructions = new()
            {
                Start = new Start { x = 0, y = 0, },
                Commands = new Command[] {
                    new Command { Direction = DirectionEnum.East, Steps = 7 },
                    new Command { Direction = DirectionEnum.West, Steps = 3 },
                    new Command { Direction = DirectionEnum.West, Steps = 0 },
                    new Command { Direction = DirectionEnum.North, Steps = 5 },
                    new Command { Direction = DirectionEnum.South, Steps = 14 }
                },
            };

            var cleaningResult = RouteCalculator.PerformCleaning(instructions);

            Assert.Equal(22, cleaningResult.Result);
        }

        [Fact (Skip = "broken with parallel.For")]
        public void PerformCleaning_Benchmark_PerformanceOk()
        {
            CleaningInstructionsModel instructions = new();
            instructions.Start = new Start { x = -100000, y = 0 };

            var commandArr = new Command[10000];

            commandArr[0] = new Command
            {
                Direction = DirectionEnum.East,
                Steps = 200000
            }; 

            for (int i = 1; i <= 9999; i++)
            {
                Command command = new();
                command.Steps = 200000;
                command.Direction = i % 2 == 0 ? DirectionEnum.East : DirectionEnum.West;
                commandArr[i] = command;
            }

            instructions.Commands = commandArr;

            var cleaningResult = RouteCalculator.PerformCleaning(instructions);

            Assert.True(cleaningResult.Duration < 5);
        }
    }
}
