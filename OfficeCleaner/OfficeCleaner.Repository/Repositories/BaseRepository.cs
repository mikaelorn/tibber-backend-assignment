﻿using Npgsql;
using OfficeCleaner.Repository.Interfaces;
using System.Threading.Tasks;

namespace OfficeCleaner.Repository.Repositories
{
    public class BaseRepository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        protected NpgsqlConnection connection;

        public BaseRepository(IDatabaseContext databaseContext)
        {
            connection = databaseContext.GetContext();
        }

        //TODO add should prod use ExecuteNonQuery but couldnt get inserted entity back even with RETURNS in sql
        public async Task<TEntity> Add(NpgsqlCommand cmd)
        {
            await connection.OpenAsync();
            await cmd.PrepareAsync();
            var result = await cmd.ExecuteScalarAsync();
            await connection.CloseAsync();
            return (TEntity)result;
        }
    }
}
