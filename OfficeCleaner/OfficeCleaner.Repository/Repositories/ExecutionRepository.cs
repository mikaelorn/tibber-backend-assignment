﻿using Npgsql;
using OfficeCleaner.Repository.Interfaces;
using OfficeCleaner.Repository.Models;
using System;
using System.Threading.Tasks;

namespace OfficeCleaner.Repository.Repositories
{
    public class ExecutionRepository : BaseRepository<Execution>, IExecutionRepository
    {
        public ExecutionRepository(IDatabaseContext databaseContext) 
            : base(databaseContext) {}

        public async Task<Execution> Add(Execution result)
        {
            result.ExecutionTimestamp = DateTime.Now;

            //NpgSql prevents SQL injection 
            await using NpgsqlCommand cmd = new(
                @"INSERT INTO executions 
                (execution_timestamp, commands, result, duration) 
                VALUES(@execution_timestamp, @commands, @result, @duration)"
            , connection);

            cmd.Parameters.AddWithValue("execution_timestamp", result.ExecutionTimestamp);
            cmd.Parameters.AddWithValue("commands", result.Commands);
            cmd.Parameters.AddWithValue("result", result.Result);
            cmd.Parameters.AddWithValue("duration", result.Duration);

            var dbResult = await Add(cmd);
            return result;
        }
    }
}
