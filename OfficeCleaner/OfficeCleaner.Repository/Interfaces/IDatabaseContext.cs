﻿using Npgsql;

namespace OfficeCleaner.Repository.Interfaces
{
    public interface IDatabaseContext
    {
        NpgsqlConnection GetContext();
    }
}
