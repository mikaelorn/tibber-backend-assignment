﻿using Npgsql;
using System.Threading.Tasks;

namespace OfficeCleaner.Repository.Interfaces
{
    public interface IRepository<TEntity> where TEntity : class
    {
         Task<TEntity> Add(NpgsqlCommand command);
    }
}
