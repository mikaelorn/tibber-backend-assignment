﻿using OfficeCleaner.Repository.Models;
using System.Threading.Tasks;

namespace OfficeCleaner.Repository.Interfaces
{
    public interface IExecutionRepository
    {
        Task<Execution> Add(Execution result);        
    }
}

