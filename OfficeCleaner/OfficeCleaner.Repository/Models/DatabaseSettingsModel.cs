﻿
namespace OfficeCleaner.Repository.Models
{
    public class DatabaseSettingsModel
    {
        public string Username { get; set; }
        public string Dbpassword { get; set; }
        public string Host { get; set; }
        public int Port { get; set; }
        public string Database { get; set; }
    }
}