﻿using System;

namespace OfficeCleaner.Repository.Models
{
    public class Execution
    {
        public int Id { get; set; }
        public int Commands { get; set; }
        public int Result { get; set; }
        public decimal Duration { get; set; }
        public DateTime ExecutionTimestamp { get; set; }
    } 
}
