﻿using Microsoft.Extensions.Options;
using Npgsql;
using OfficeCleaner.Repository.Interfaces;
using OfficeCleaner.Repository.Models;

namespace OfficeCleaner.Repository
{
    /// <summary>
    /// Since we dont use EF this is a custom context that can be injected as 
    /// Singleton
    /// </summary>
    public class DatabaseContext : IDatabaseContext
    {
        private readonly NpgsqlConnection context;

        public DatabaseContext(IOptions<DatabaseSettingsModel> settings)
        {
            context = new NpgsqlConnection(new NpgsqlConnectionStringBuilder
            {
                Host     = settings.Value.Host,
                Port     = settings.Value.Port,
                Database = settings.Value.Database,
                Username = settings.Value.Username,
                Password = settings.Value.Dbpassword
            }.ConnectionString);
        }

        public NpgsqlConnection GetContext()
        {
            return context;
        }
    }
}

