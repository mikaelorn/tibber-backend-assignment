CREATE TABLE executions (
    id bigserial primary key,
    execution_timestamp timestamp with time zone,
    commands integer,
    result integer,
    duration double precision
);